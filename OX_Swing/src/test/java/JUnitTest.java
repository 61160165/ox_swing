
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import OOP.player;

public class JUnitTest {
    
    public JUnitTest() {
    }

    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    @Test
    public void testWin(){
        player o =new player();
        o.setPlayer('O');
        assertEquals(0,o.getWin());
    }
    public void testPlayer(){
        player o =new player();
        o.setPlayer('O');
        assertEquals('O',o.getPlayer());
    }
    
    public void testLose(){
        player o =new player();
        o.setPlayer('O');
        assertEquals(0,o.getLose());
    }
    public void testDraw(){
        player o =new player();
        o.setPlayer('O');
        o.Draw();
        assertEquals(1,o.getDraw());
    }
    
    public void testScore(){
        player o =new player();
        o.setPlayer('O');
        o.Draw();
        o.Win();
        String score = o.getScore();
        assertEquals(score,o.getScore());
    }
    
}
