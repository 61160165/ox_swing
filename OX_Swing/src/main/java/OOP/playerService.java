/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OOP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class playerService {
    static  player o,x;
    static {
        o = new player('O');
        x = new player('X');
    }
    public static void load(){
        File file  = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("oxt.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            o = (player) ois.readObject();
            x = (player) ois.readObject();
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } catch (ClassNotFoundException ex) {
            
        }
        System.out.println(o);
        System.out.println(x);
    }
    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            file = new File("oxt.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (oos!=null) oos.close();
                if (fos!=null) fos.close();
            } catch (IOException ex) {
                Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static player getO(){
        return o;
    }
    public static player getX(){
        return x;
    }
    
}
