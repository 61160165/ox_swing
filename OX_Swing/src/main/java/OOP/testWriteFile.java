package OOP;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class testWriteFile {
    public static void main(String[] args){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        player o = new player('O');
        player x = new player('X');
        o.Win();
        x.Lose();
        x.Win();
        o.Lose();
        o.Draw();
        x.Draw();
        try{
            file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (oos!=null) oos.close();
                if (fos!=null) fos.close();
            } catch (IOException ex) {
                Logger.getLogger(testWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
